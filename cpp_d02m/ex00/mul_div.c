void add_mul_4param(int first, int second, int *sum, int *product)
{
  sum[0] = first + second;
  product[0] = first * second;
}

void add_mul_2param(int *first, int *second)
{
  int nb = *first;

  first[0] = *first + *second;
  second[0] = nb * *(second);
}
