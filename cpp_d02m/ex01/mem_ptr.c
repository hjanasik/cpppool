#include "mem_ptr.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void add_str(char *str1, char *str2, char **res)
{
  int size1 = strlen(str1);
  int size2 = strlen(str2);
  int i = 0;
  char *result = NULL;

  result = malloc(sizeof(char) * (size1 + size2 + 1));
  if (result == NULL)
    exit (84);
  result[size1 + size2] = 0;  
  for ( ; str1[i] ; i++)
    result[i] = str1[i];
  for (int j = 0 ; str2[j] ; j++)
    result[size1 + j] = str2[j];
  res[0] = result;
}

void add_str_struct(str_op_t *str_op)
{
  int size1 = strlen(str_op->str1);
  int size2 = strlen(str_op->str2);
  int i = 0;
  char *result = NULL;

  result = malloc(sizeof(char) * (size1 + size2 + 1));
  if (result == NULL)
    exit (84);
  result[size1 + size2] = 0;
  for ( ; str_op->str1[i] ; i++)
    result[i] = str_op->str1[i];
  for (int j = 0 ; str_op->str2[j] ; j++)
    result[size1 + j] = str_op->str2[j];
  str_op->res = result;
}

static void test_add_str(void)
{
  char *str1 ="Hey, ";
  char *str2 ="it works !";
  char *res;

  add_str(str1, str2, &res);
  printf("%s\n", res);
}

static void test_add_str_struct(void)
{
  char *str1 ="Hey, ";
  char *str2 ="it works !";
  str_op_t str_op;

  str_op.str1 = str1;
  str_op.str2 = str2;
  add_str_struct(&str_op);
  printf("%s\n", str_op.res);
}

int main (void)
{
  test_add_str();
  test_add_str_struct();
  return (0);
}
