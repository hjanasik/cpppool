#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "func_ptr_enum.h"

void print_normal(char *str)
{
  printf("%s\n", str);
}

void print_reverse(char *str)
{
  int size1 = strlen(str);
  char *res = malloc(sizeof(char) * (size1 + 1));
  int j = 0;

  if (res == NULL)
    exit (84);
  res[size1] = 0;
  for (int i = size1 - 1; i >= 0; i--) {
    res[j] = str[i];
    j++;
  }
  printf("%s\n", res);
}

void print_upper(char *str)
{
  int size = strlen(str);
  char *res = malloc(sizeof(char) * (size + 1));

  if (res == NULL)
    exit (84);
  for (int i = 0 ; str[i] ; i++) {
    if (str[i] >= 97 && str[i] <= 122) {
      res[i] = str[i] - 32;
      continue;
    }
    res[i] = str[i];
  }
  printf("%s\n", res);
}

void print_42(char *str)
{
  (void)str;
  printf("42\n");
}

void do_action(action_t action, char *str)
{
  
}

int main(void)
{
  char *str = "I'm Using function pointers!";

  do_action(PrINT_NOrMAL, str);
  do_action(PrINT_rEVErSE ,str);
  do_action(PrINT_UPPEr, str);
  do_action(PrINT_42, str);
  return (0);
}
