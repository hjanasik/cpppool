#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include "bitmap.h"

void make_bmp_info_header(bmp_info_header_t *header, size_t size)
{
        header->size = 40;
	header->width = size;
	header->height = size;
	header->planes = 1;
	header->bpp = 32;
	header->compression = 0;
	header->raw_data_size = size * size * 4;
	header->h_resolution = 0;
	header->v_resolution = 0;
	header->palette_size = 0;
	header->important_colors = 0;
}

void make_bmp_header(bmp_header_t *header, size_t size)
{
	header->magic = 0x4D42;
	header->size = size * size * 4 + 54;
	header->_app1 = 0;
	header->_app2 = 0;
	header->offset = 54;
}

void write_bmp_header(int fd)
{
	bmp_header_t header;

	make_bmp_header(&header, 32);
	write(fd, &header, sizeof(header));
}

void write_bmp_info_header(int fd)
{
	bmp_info_header_t info;

	make_bmp_info_header(&info, 32);
	write(fd, &info, sizeof(info));
}

void write_32bits(int fd, unsigned int pixel)
{
	for (size_t i = 0 ; i < 32 * 32 ; ++i)
	    write(fd, &pixel, sizeof(pixel));
}

int main(void)
{
	int fd = open("32px.bmp", O_CREAT | O_TRUNC | O_WRONLY, 0644);

	if (fd < 0)
	   return (84);
	write_bmp_header(fd);
	write_bmp_info_header(fd);
	write_32bits(fd, 0x00FFFFFF);
	close(fd);
	return (EXIT_SUCCESS);
}
