#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
 
#include "bitmap.h"
#include "menger.h"
#include "drawing.h"

int main(int argc, char **argv)
{
  if (argc != 4) {
    printf("menger_face file_name size level\n");
    return (84);
  }
  char *name_file = argv[1];
  size_t size = atoi(argv[2]);
  int level = atoi(argv[3]);
  unsigned int *buffer = malloc(size * size * sizeof(*buffer));
  unsigned int **img = malloc(size * sizeof(*img));

  create_image(size ,buffer ,img);
  create_bitmap_from_buffer(size ,buffer, name_file);
  return 0;
}
