#include "menger.h"
#include <stdio.h>

void left(int level, int size, int x, int y)
{
  menger(level, size, 2 / size + x, 2 / size + y);//en haut a gauche
  menger(level, size, 2 / size + x, size + y);//au milieu a gauche
  menger(level, size, 2 / size + x, 2 * size + y);//en bas a gauche
}

void middle(int level, int size, int x, int y)
{
  menger(level, size, size + x, 2 / size + y);//en haut au milieu
  menger(level, size, size + x, 2 * size + y);//en bas au milieu
}

void right(int level, int size, int x, int y)
{
  menger(level, size, 2 * size + x, 2 / size + y);//en haut a droite
  menger(level, size, 2 * size + x, size + y);//au milieu a droite
  menger(level, size, 2 * size + x, 2 * size + y);//en bas a droite
}

void menger(int level, int size, int x, int y)
{
  size = size / 3;
  printf("%03d %03d %03d\n", size, x + size, y + size);

  if (level > 1 && size / 3 > 0) {
    level -= 1;
    left(level, size, x, y);
    middle(level, size, x, y);
    right(level, size, x, y);
  }
}
