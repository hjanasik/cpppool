#include <stdint.h>

typedef struct point {
  unsigned int x;
  unsigned int y;
}point_t;

void draw_square(uint32_t **, point_t *, size_t, uint32_t);
void initialize_image(size_t, unsigned int *, unsigned int **);
void create_image(size_t, unsigned int *, unsigned int **);
void create_bitmap_from_buffer(size_t, unsigned int *, char *);
