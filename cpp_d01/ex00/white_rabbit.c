#include <stdlib.h>
#include <stdio.h>


int follow_the_white_rabbit(void)
{
  int nb_tot = 0;
  int nb = 0;
 
  while (1) {
    nb = random() % 37 + 1;
    if (nb == 37 || nb_tot >= 397 || nb == 1) {
      printf("RABBIT!!!\n");
      return (nb_tot);
    }
    else if ((nb >= 4 && nb <= 6) || (nb >= 17 && nb <= 21) || nb == 28)
      printf("left\n");
    else if (nb == 13 || nb >= 34)
      printf("right\n");
    else if (nb == 10 || nb == 15 || nb == 23)
      printf("forward\n");
    else if (nb > 24 || nb % 8 == 0)
      printf("backward\n");
    nb_tot += nb;
  }
  return (nb_tot);
}

int main()
{
  return(follow_the_white_rabbit());
}
