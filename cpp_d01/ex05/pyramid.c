int pyramid_path(int size, int **map)
{
  int i = 0;
  int j = 0;
  int tot = 0;

  if (map == NULL)
    return (0);
  tot = map[j][i];
  for ( ; j + 1 < size ; j++) {
    if (map[j + 1][i] > map[j + 1][i + 1]) {
      tot += map[j + 1][i + 1];
      i++;
    } else
      tot += map[j + 1][i];
  }
  return (tot);
}
