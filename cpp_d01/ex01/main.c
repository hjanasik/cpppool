#include "menger.h"
#include <stdlib.h>

int my_getnbr(char const *str)
{
	int i = 0;
	int nb = 0;
	int neg = 0;
	int comp = 1;

	if (str == NULL)
		return (0);
	if (str[0] < '0' || str[0] > '9')
		return (0);
	while (str[i] < '0' || str[i] > '9') {
		i = i + 1;
	}
	if (str[i] <= '9' && str[i] >= '0') {
		while (str[i - comp] == '-') {
			comp += 1;
			neg += 1;
		}
	}
	while (str[i] <= '9' && str[i] >= '0') {
		nb = nb + str[i] - 48;
		nb = nb * 10;
		i = i + 1;
	}
	if (neg % 2 != 0)
		nb *= -1;
	return (nb / 10);
}

int main(int argc, char **argv)
{
  int level = 0;
  int size = 0;

  if (argc != 3)
    return (84);
  size = my_getnbr(argv[1]);
  level = my_getnbr(argv[2]);
  if (size % 3 != 0 || level > size / 3 || size == 0 || level == 0)
    return (84);
  menger(level, size, 0, 0);
  return (0);
}
